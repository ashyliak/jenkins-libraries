def notifySlack(def buildStatus = 'STARTED', def deployedEnv) {
    // Build status of null means success.
    buildStatus = buildStatus ?: 'SUCCESS'

    def color

    if (buildStatus == 'STARTED') {
        color = '#D4DADF'
    } else if (buildStatus == 'SUCCESS') {
        color = '#42B777'
    } else if (buildStatus == 'UNSTABLE') {
        color = '#FFFE89'
    } else {
        color = '#DF0024'
    }
    def msg = "*${buildStatus}:* *${env.JOB_NAME}* \n *Build link:* ${env.BUILD_URL} \n *Deployed on:* `${deployedEnv}`"

    slackSend(color: color, channel: "#tests-dummy", message: msg)
}